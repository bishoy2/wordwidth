package edu.westga.wordwitdth.model;

public class StringUtils {
	
	/**
	 * Finds the "width" of a word according to the following criteria:
	 * <ul>
	 * <li>'i' and 'j' are worth 1
	 * <li> 'm' is worth 3
	 * <li> all other lowercase letters are worth 2
	 * <li> 'I' is worth 2
	 * <li> 'M' is worth 4
	 * <li> all other uppercase letters are worth 3
	 * </ul>
	 * <p>if the word contains any non-alphabetic character an IllegalArgumentException is thrown</p>
	 * @param word
	 * @return
	 */
	public static int findWordWidth(String word) {
		if (word == null || word.matches(".*\\d+.*")) {
			throw new IllegalArgumentException("word should not be null");
		}
		if (word.isEmpty()) {
			return 0;
		}
		int sum = 0;
		for (char ltr: word.toCharArray()) {
			if (ltr == 'i' || ltr == 'j') {
				sum += 1;
			} else if (ltr == 'm') {
				sum += 3;
			} else if (Character.isLowerCase(ltr) || ltr == 'I') {
				sum += 2;
			} else if (ltr == 'M') {
				sum += 4;
			} else if (Character.isUpperCase(ltr)) {
				sum += 3;
			}
		}
		
		return sum;
	}
}
