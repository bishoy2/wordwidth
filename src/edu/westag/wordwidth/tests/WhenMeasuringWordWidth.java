package edu.westag.wordwidth.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.westga.wordwitdth.model.StringUtils;

public class WhenMeasuringWordWidth {
	
	@Test(expected = IllegalArgumentException.class)
	public void nullWordShouldThrowException() {
	   StringUtils.findWordWidth(null);
	}
	
	@Test
	public void emptyWordShouldReturnZero() {
	   assertEquals(0, StringUtils.findWordWidth(""));
	}
	
	@Test
	public void jimijamShouldHaveWidthOf12() {
	   int width = StringUtils.findWordWidth("jimijam");
	   assertEquals(12, width);
	}
	
	@Test
	public void jimiJAMShouldHaveWidthOf16() {
		assertEquals(16, StringUtils.findWordWidth("jimiJAM"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void wordWithNonAlphabeticCharacterShouldThrowException() {
		StringUtils.findWordWidth("123??\\''");
	}
	
	@Test
	public void JIMIJAMShouldHaveWidthOf21() {
		assertEquals(21, StringUtils.findWordWidth("JIMIJAM"));
	}
}
